package VFS;

use utf8;
use strict;
use warnings;
use 5.010;
use JSON::XS;
use Encode qw(decode encode);

no warnings 'experimental::smartmatch';

my %type = (
    file        =>  1,
    directory   =>  2
);
my $buf;

sub mode2s {
    my $file_permissions = shift;
    return {
        other => {
            execute => $file_permissions & 1    ? JSON::XS::true : JSON::XS::false,
            write   => $file_permissions & 2    ? JSON::XS::true : JSON::XS::false,
            read    => $file_permissions & 4    ? JSON::XS::true : JSON::XS::false,
        },
        group => {
            execute => $file_permissions & 8    ? JSON::XS::true : JSON::XS::false,
            write   => $file_permissions & 16   ? JSON::XS::true : JSON::XS::false,
            read    => $file_permissions & 32   ? JSON::XS::true : JSON::XS::false,
        },
        user => {
            execute => $file_permissions & 64   ? JSON::XS::true : JSON::XS::false,
            write   => $file_permissions & 128  ? JSON::XS::true : JSON::XS::false,
            read    => $file_permissions & 256  ? JSON::XS::true : JSON::XS::false,
        }
    }
}

sub parse {
    $buf = shift;
    my $type = unpack "A", $buf;

    if ($type eq 'Z') {
        return {};
    } elsif ($type ne 'Z' and $type ne 'D') {
        die("The blob should start from 'D' or 'Z'");
    }
    my $list = create_list();
    return $list->[0];
}

sub create_node {
    my $node_type = shift;
    my (%node, $name, $file_permissions);
    if ($node_type == $type{file}) {
        ($name, $file_permissions, $node{size}, my $sha1, $buf) = unpack "n/A* n N A20 A*", $buf;
        $node{type} = "file";
        $node{hash} = unpack "H*", $sha1;
    } elsif ($node_type == $type{directory}) {
        ($name, $file_permissions, $buf) = unpack "n/A* n A*", $buf;
        $node{type} = "directory";
    }
    $node{name} = decode('utf-8', $name);
    $node{mode} = mode2s($file_permissions);
    return \%node;
}

sub create_list {
    my @list;
    while ($buf) {
        my $command = unpack "A", $buf;
        $buf = substr $buf, 1;
        given ($command) {
            when ('D')  { push @list, create_node($type{directory}) }
            when ('F')  { push @list, create_node($type{file}) }
            when ('I')  { $list[-1]->{list} = create_list() }
            when ('U')  { return \@list }
            when ('Z')  { die("Garbage ae the end of the buffer") if ($buf); return \@list }
            default     { die "Unknown command! Exit..." }
        }
    }
}

1;
